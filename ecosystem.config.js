module.exports = {
    apps: [
        {
            name: "CI-SLACK-BOT",
            script: "./bundle/index.js",
            instances: 2,
            max_memory_restart: "300M",

            // Logging
            out_file: "./out.log",
            error_file: "./error.log",
            merge_logs: true,
            log_date_format: "DD-MM HH:mm:ss Z",
            log_type: "json",

            // Env Specific Config
            env_production: {
                NODE_ENV: "production",
                PORT: 9998,
                exec_mode: "cluster_mode",
            },
            env_development: {
                NODE_ENV: "development",
                PORT: 9998,
                watch: true,
                watch_delay: 3000,
                ignore_watch: [
                    "./node_modules",
                    "./public",
                    "./.DS_Store",
                    "./package.json",
                    "./yarn.lock",
                    "./samples",
                    "./src"
                ],
            },
        },
    ],
    deploy: {
        production: {
            "user" : "ubuntu",
            "host" : ["192.168.1.4", "192.168.64.1"],
            "ref"  : "origin/master",
            "repo" : "git@gitlab.com:zel4/slack-merge-request-2.git",
            "path" : "/var/www/",
            "post-deploy" : "npm i && npm run build && pm2 startOrRestart ecosystem.config.js --env production"
        }
    }
};




// module.exports = {
//     apps: [
//         {
//             name: "node-cicd-pm2",
//             script: "./bundle/index.js"
//         }
//     ],
//     // Deployment Configuration
//     deploy : {
//         production : {
//             "user" : "ubuntu",
//             "host" : ["192.168.1.4", "192.168.64.1"],
//             "ref"  : "origin/master",
//             "repo" : "git@gitlab.com:zel4/slack-merge-request-2.git",
//             "path" : "/var/www/slack-merge-request-2",
//             "post-deploy" : "npm i && npm run build && pm2 startOrRestart ecosystem.config.js --env production"
//         }
//     }
// }