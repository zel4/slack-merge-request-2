import * as packages from '@slack/bolt';
import dotenv from 'dotenv';

dotenv.config();

const { App, LogLevel } = packages;
// const { addEntry } = require('./utils/data-store')

/**
 * /!\ CHALLENGE STEP /!\
 * This is required to verify the endpoint
 *
 * - Go to your app manifest in Slack app: https://app.slack.com/app-settings/T077BN7F30U/A077Y282ADN/app-manifest
 * - replace socket_mode_enabled: false
 * - replace event_subscriptions: request_url: https://www.domain.com/slack/events
 * - replace interactivity: request_url: https://www.domain.com
 * - replace slash_commands: request_url: https://www.domain.com/commands
 * - Save changes
 * - The head of the page will display a message with a link to the challenge, wait before click. (other methods: Go to the Slack app -> Features -> Event Subscriptions -> Enable Events -> Request URL: https://www.domain.com/slack/events)
 * - Go to your app code
 * - Change socketMode to false
 * - start the app
 * - Click on the link in the Slack app
 * - The app will receive a challenge event
 * - stop app
 * - Change socketMode to true
 * - start the app
 * - Go to your app manifest in Slack app: https://app.slack.com/app-settings/T077BN7F30U/A077Y282ADN/app-manifest
 * - replace socket_mode_enabled: true
 */

/**
 * /!\ IMPORTANT INFORMATION /!\
 * respond(): Send message from the app, respond un can only be used in the command
 * say(): Send message from the bot
 * client.chat.postMessage(): Send message from the bot
 */

const imageSuccess =
    'https://gitlab.com/assets/ci_favicons/favicon_status_success-8451333011eee8ce9f2ab25dc487fe24a8758c694827a582f17f42b0a90446a2.png'
const app = new App({
      token: process.env.SLACK_GITLAB_MR_BOT_TOKEN, // Slack bot token or user token
      signingSecret: process.env.SLACK_GITLAB_MR_SIGNING_SECRET,
      appToken: process.env.SLACK_GITLAB_MR_APP_TOKEN,
      socketMode: true, // Set at false if you want update subscription event endpoint https://www.domain.com/slack/events
      logLevel: LogLevel.INFO,
    })

;(async () => {
  // Start the app
  await app.start(9998)
  console.log('⚡️ Bolt app is running!')
})()

import {
  getMergeRequestById,
  approveMergeRequestById,
  mergedMergeRequestById,
  getPipelineByMergeRequestId,
  filtersMergeRequestList,
} from './gitlab/gitlab'

import {
  formatMessageMergeRequest,
  truncateText,
  getMessageByChannelIdSlack,
  retrieveMergeRequestIdFromMessage,
  errorMessage,
} from './utils/utils'

/**********/
/* EVENTS */
/**********/
app.event('/slack/events', async ({ event, body, payload, client, say }) => {
  try {
    // Challenge event
    console.log('challenge:event - ', event)
    console.log('challenge:body  - ', body)
  } catch (error) {
    console.error(error)
  }
})

// subscribe to 'reaction_added' event in your App config
app.event('reaction_added', async ({ body, client, say, logger, payload }) => {
  try {
    const channel_id = payload.item.channel
    const reaction_message_ts = payload.item.ts
    const reaction = body.event.reaction

    if (reaction === 'white_check_mark' || reaction === 'eyes') {
      // On récupère le message de la merge request, pour récupérer l'id de la merge request
      const result = await getMessageByChannelIdSlack(client, channel_id)
      const messages = result.messages
      const currentMessage = messages.filter(
        // @ts-ignore
        message => message.ts === reaction_message_ts
      )
      const mergeRequestId = await retrieveMergeRequestIdFromMessage(
        currentMessage[0].blocks[0].text.text
      )

      if (mergeRequestId === null) {
        await errorMessage(say, logger, {
          channel: channel_id,
          thread_ts: reaction_message_ts,
          text: ":x: *Je n'ai pas pu trouver l'id de la merge request.*",
        })
        return
      }
      // Approve merge request
      if ('white_check_mark' === reaction) {
        await say({
          channel: channel_id,
          thread_ts: reaction_message_ts,
          text: 'Your merge request is approved.',
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `Vous êtes sur le point de merger la merge request #${mergeRequestId}.`,
              },
            },
            {
              type: 'actions',
              elements: [
                {
                  type: 'button',
                  style: 'primary',
                  text: {
                    type: 'plain_text',
                    text: `I want merge`,
                    emoji: true,
                  },
                  value: mergeRequestId,
                  action_id: `action_merge_merge_request`,
                },
              ],
            },
          ],
        })
      }
      // if ('eyes' === reaction) {
      //   await say({
      //     text: 'Merge request en cours de review.',
      //     thread_ts: reaction_message_ts,
      //   })
      // }
    }
  } catch (error) {
    logger.error('Reaction added:', JSON.stringify(error))
  }
})

/************/
/* COMMANDS */
/************/

// Send message merge request //
app.command(
    '/select-merge-request',
      // @ts-ignore
    async ({  ack, client, payload }: any) => {
    await ack()
    try {
      const private_metadata = {
        channel_id: payload.channel_id,
        user_id: payload.user_id,
        user_name: payload.user_name,
      }
      const openedMergeRequest = await filtersMergeRequestList({
        state: 'opened',
        perPage: 50,
      })
      // Order merge request by draft
      const orderedMergeRequest = [
        // @ts-ignore
        ...openedMergeRequest.filter(c => c.draft === false),
        // @ts-ignore
        ...openedMergeRequest.filter(c => c.draft === true)
      ]
      const modal = {
        type: 'modal',
        callback_id: 'callback_selected_merge_request_iid',
        title: {
          type: 'plain_text',
          text: 'Merge Request',
        },
        close: {
          type: 'plain_text',
          text: 'Cancel',
        },
        submit: {
          type: 'plain_text',
          text: 'Send',
        },
        blocks: [
          {
            block_id: 'select_merge_request',
            type: 'input',
            element: {
              type: 'static_select',
              placeholder: {
                type: 'plain_text',
                text: 'Select an item',
                emoji: true,
              },
              options: orderedMergeRequest.map(mr => ({
                text: {
                  type: 'plain_text',
                  text: `#${mr.iid} - ${truncateText(mr.title, 50)}`,
                  emoji: true,
                },
                value: mr.iid.toString(),
              })),
              action_id: 'action_select_merge_request_iid',
            },
            label: {
              type: 'plain_text',
              text: 'Select Merge Request',
              emoji: true,
            },
          },
        ],
        private_metadata: JSON.stringify(private_metadata),
      }
      const modalInfo = await client.views.open({
        trigger_id: payload.trigger_id,
        view: modal,
      })
      // console.log(
      //   'getFilesDiffByMergeRequestId',
      //   JSON.stringify(await getFilesDiffByMergeRequestId(globalMergeRequestId))
      // )

      if (modalInfo.error) {
        const error = `Failed to open a modal in the workflow. Contact the app maintainers with the following information - (error: ${modalInfo.error})`
        return { error }
      }
    } catch (error) {
      console.error(error)
    }
  }
)

/*********/
/* VIEWS */
/*********/

app.view(
  {
    callback_id: 'callback_selected_merge_request_iid',
    type: 'view_submission',
  },
  async ({ ack, body, view, client }: any) => {
    await ack()

    const private_metadata = JSON.parse(view.private_metadata)

    const selectedMergeRequestIid =
      view?.state?.values?.select_merge_request?.action_select_merge_request_iid?.selected_option?.value as string

    const mergeRequest = await getMergeRequestById(selectedMergeRequestIid)

    try {
      const response = await client.chat.postMessage({
        channel: private_metadata.channel_id,
        text: `${body?.user?.username} a posté une nouvelle merge request !`,
        blocks: formatMessageMergeRequest(
          mergeRequest,
          private_metadata,
          selectedMergeRequestIid
        ).blocks,
      })
      // addEntry({
      //   user_id: private_metadata.user_id,
      //   ts: response.ts,
      //   channel_id: response.channel,
      //   merge_request_iid: selectedMergeRequestIid,
      // })
    } catch (error) {
      console.error(error)
    }
  }
)

/***********/
/* ACTIONS */
/***********/

// Approve merge request
app.action(
  'action_approve_merge_request',
  async ({ payload, ack, body, client, say, respond, action }: any) => {
    try {
      // Accuser réception de la commande
      await ack()

      const isApproved = await getMergeRequestById(payload.value).then(mr => {
        if (mr.has_conflicts || mr.detailed_merge_status !== 'mergeable') {
          return false
        }
        return true
      })
      if (!isApproved) {
        // body.message.blocks[3].elements.push({
        //   type: 'button',
        //   style: 'primary',
        //   action_id: 'action_merge_merge_request',
        //   text: { type: 'plain_text', text: 'Merge', emoji: true },
        //   value: payload.value,
        // })
        await respond({
          channel: body.container.channel_id,
          blocks: body.message.blocks,
          ts: body.container.message_ts,
          replace_original: true,
        })
      }
      await approveMergeRequestById(payload.value)
    } catch (error: any) {
      console.error(error)
    }
  }
)

// Merge merge request //
app.action(
  'action_merge_merge_request',
  async ({ ack, body, say, action, logger }: any) => {
    await ack()
    try {
      // Check state of merge request, if it's mergeable, merge it, else send a message to the user
      const messageTs = body.container.thread_ts
      const channelId = body.channel.id
      const mergeRequestId = action.value

      const { hasConflicts, detailedMergeStatus, isDraft } =
        await getMergeRequestById(mergeRequestId).then(mr => {
          return {
            title: mr.title,
            hasConflicts: mr.has_conflicts,
            detailedMergeStatus: mr.detailed_merge_status,
            isDraft: mr.draft,
          }
        })
      let messageTitle = ` `
      let messageBody = ' '
      const isNotMergeable =
        hasConflicts ||
        isDraft ||
        // detailedMergeStatus !== 'can_be_merged' ||
        detailedMergeStatus !== 'mergeable'
      if (!isNotMergeable) {
        messageTitle = `:white_check_mark: The merge request can be merge`
      } else {
        messageTitle = `:warning: The merge request cannot be merge ! :warning:`
      }
      if (isDraft) {
        messageBody += `• The merge request is draft ! \n`
      }
      if (detailedMergeStatus === 'not_approved') {
        messageBody += `• The merge request was not approved ! \n`
      }
      if (hasConflicts) {
        messageBody += `• The merge request has conflicts !`
      }

      if (isNotMergeable) {
        await say({
          channel: channelId,
          thread_ts: messageTs,
          text: `${messageTitle}\n\n${messageBody}`,
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: messageTitle,
              },
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: messageBody,
              },
            },
            {
              type: 'actions',
              elements: [
                {
                  type: 'button',
                  style: 'primary',
                  text: {
                    type: 'plain_text',
                    text: 'Retry merge',
                    emoji: true,
                  },
                  value: mergeRequestId,
                  action_id: `action_merge_merge_request`,
                },
              ],
            },
          ],
        })
      } else {
        // TODO: Add error response if the merge request cannot be merged
        await mergedMergeRequestById(mergeRequestId)
        await say({
          channel: channelId,
          thread_ts: messageTs,
          text: `Merge request merged`,
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: ':white_check_mark: *Merge request merged*',
              },
            },
            // {
            //   type: 'actions',
            //   elements: [
            //     {
            //       type: 'button',
            //       text: {
            //         type: 'plain_text',
            //         text: 'Pipeline status',
            //         emoji: true,
            //       },
            //       value: mergeRequestId,
            //       action_id: `action_pipeline_merge_request`,
            //     },
            //   ],
            // },
          ],
        })
      }
    } catch (error) {
      await say({
        text: `:warning: I can't merge. \n ${JSON.stringify(error)}`,
        thread_ts: body.message.ts,
      })
      throw error
    }
  }
)

// Pipeline status merge request
app.action(
  'action_pipeline_merge_request',
  async ({ ack, body, client, say, payload, action, logger }: any) => {
    await ack()
    try {
      const messageTs = body.message.ts
      const channelId = body.channel.id
      let statusMessage = ''
      const mergeRequestId = action.value

      const pipelines = await getPipelineByMergeRequestId(mergeRequestId)
      const contextList: any[] = []
      // @ts-ignore
      pipelines.forEach(pipeline => {
        statusMessage += `${pipeline.status === 'success' ? ':x:' : ':white_check_mark:'} <${pipeline.web_url}|${pipeline.id}> - _Last update: ${pipeline.updated_at}_\n`
        contextList.push({
          type: 'context',
          elements: [
            {
              type: 'image',
              image_url: imageSuccess,
              alt_text: pipeline.status,
            },
            {
              type: 'mrkdwn',
              text: `<${pipeline.web_url}|${pipeline.id}> - _Last update: ${pipeline.updated_at}_`,
            },
          ],
        })
      })
      await say({
        channel: channelId,
        thread_ts: messageTs,
        text: `*Pipeline status:* ${mergeRequestId}`,
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: '*Pipeline status:*',
            },
          },
          ...contextList,
          // {
          //   type: 'actions',
          //   elements: [
          //     {
          //       type: 'button',
          //       text: {
          //         type: 'plain_text',
          //         text: 'Pipeline status',
          //         emoji: true,
          //       },
          //       value: mergeRequestId,
          //       action_id: `action_pipeline_merge_request`,
          //     },
          //   ],
          // },
        ],
      })
    } catch (error) {
      logger.error('action_pipeline_merge_request', error)
      throw error
    }
  }
)
