export const badgeStagingSuccess =
  'https://shields.io/badge/BUILD-green?style=flat-square&label=STAGING'
export const testStagingSuccess =
  'https://shields.io/badge/TEST-green?style=flat-square&label=STAGING'
export const terraformStagingSuccess =
  'https://shields.io/badge/TERRAFORM-green?style=flat-square&label=STAGING'
export const installStagingSuccess =
  'https://shields.io/badge/INSTALL-green?style=flat-square&label=STAGING'
