import { Logger } from "@slack/bolt";

export const truncateText = (text:string, maxLength: number) => {
  if (text.length <= maxLength) {
    return text
  }
  return text.slice(0, maxLength - 3) + '...'
}

export const getMessageByChannelIdSlack = async (client: any, channelId: string) => {
  try {
    return client.conversations.history({
      channel: channelId,
      limit: 100,
    })
  } catch (error) {
    console.error('Error getting message:', error)
    return { error: error }
  }
}

export const formatMessageMergeRequest = (
  mergeRequest: any,
  private_metadata: any,
  selectedMergeRequestIid: string
) => {
  return {
    blocks: [
      {
        type: 'header',
        text: {
          type: 'plain_text',
          text: `#${mergeRequest.iid} - ${mergeRequest.title}`,
          emoji: true,
        },
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `_From <@${private_metadata.user_name}> requested to merge *${mergeRequest.source_branch}* to *${mergeRequest.target_branch}*_`,
        },
      },
      {
        type: 'divider',
      },
      {
        type: 'context',
        elements: [
          {
            type: 'mrkdwn',
            text: `*Description:*\n${formatMarkdownGitlabToSlack(mergeRequest.description)}`,
          },
        ],
      },
      {
        type: 'divider',
      },
      {
        type: 'actions',
        elements: [
          {
            type: 'button',
            style: 'primary',
            text: {
              type: 'plain_text',
              text: 'View',
              emoji: true,
            },
            value: 'view_merge_request',
            url: mergeRequest.web_url,
            action_id: `action_view_merge_request`,
          },
          // TODO : Add a check if the user is the author of the merge request
          // {
          //   type: 'button',
          //   text: {
          //     type: 'plain_text',
          //     text: 'Pipeline status',
          //     emoji: true,
          //   },
          //   value: selectedMergeRequestIid,
          //   action_id: `action_pipeline_merge_request`,
          // },
        ],
      },
    ],
  }
}

export const formatMarkdownGitlabToSlack = (text: string) => {
  let finalText = ''
  const regexTitle = /^## (.+)$/gm
  finalText = text.replace(regexTitle, '*$1*')
  return finalText
}

export const retrieveMergeRequestIdFromMessage = async (message: any) => {
  const pattern = /^.{0,5}#(\d+)/g
  const match = await pattern.exec(message)
  if (match !== null) {
    return match?.[1]
  }
  return null
}

export const errorMessage = async (say: any, logger: Logger, params: any) => {
  await say({
    channel: params.channelId,
    thread_ts: params.messageTs,
    text: 'Error Gitlab MR.',
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: params.text,
        },
      },
    ],
  })
  return true
}
