import axios from 'axios'

import dotenv from 'dotenv';

dotenv.config();

const gitlabToken: string = process.env.GITLAB_MR_TOKEN || ''
const gitlabApiUrl: string = process.env. GITLAB_MR_API_URL || ''
const projectId: string = process.env.GITLAB_MR_PROJECT_ID || ''

export const baseUrlImage = `https://gitlab.com/-/project/${projectId}`

export const getMergeRequestById = async (mergeRequestId: string) => {
  try {
    const response = await axios.get(
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}`,
      {
        headers: {
          'Private-Token': gitlabToken,
        },
      }
    )

    return response.data
  } catch (error) {
    // console.error(
    //   `Error fetching merge request: ${error.response.status} - ${error.response.statusText}`
    // )
    throw error
  }
}

export const getPipelineByMergeRequestId = async (mergeRequestId: string) => {
  try {
    const response = await axios.get(
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/pipelines`,
      {
        headers: {
          'Private-Token': gitlabToken,
        },
      }
    )

    return response.data
  } catch (error: any) {
    console.error(
      `Error fetching merge request: ${error.response.status} - ${error.response.statusText}`
    )
    console.error(error.response.data)
    throw error
  }
}

export const getFilesDiffByMergeRequestId = async (mergeRequestId: string) => {
  try {
    const response = await axios.get(
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/diffs`,
      {
        headers: {
          'Private-Token': gitlabToken,
        },
      }
    )

    return response.data
  } catch (error: any) {
    console.error(
      `Error fetching merge request: ${error.response.status} - ${error.response.statusText}`
    )
    console.error(error.response.data)
    throw error
  }
}

export const filtersMergeRequestList = async (params: any = {}) => {
  try {
    const filterParams = new URLSearchParams()

    // Date
    if (params.startDate) {
      filterParams.append('created_after', params.startDate)
    }
    if (params.endDate) {
      filterParams.append('created_before', params.endDate)
    }

    // Branches
    if (params.sourceBranch) {
      filterParams.append('source_branch', params.sourceBranch)
    }
    if (params.targetBranch) {
      filterParams.append('target_branch', params.targetBranch)
    }

    // Scope & State
    filterParams.append('scope', params.scope ? params.scope : 'all')
    if (params.state) {
      filterParams.append('state', params.state)
    }

    // Pagination
    if (params.perPage) {
      filterParams.append('per_page', params.perPage)
    }

    const response = await axios.get(
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests?${filterParams.toString()}`,
      {
        headers: {
          'Private-Token': gitlabToken,
        },
      }
    )
    return response.data
  } catch (error) {
    console.error(
      `Error fetching merge request: ${error}`
    )
    throw error
  }
}

export const approveMergeRequestById = async (mergeRequestId: string) => {
  try {
    console.log(
      'Approving merge request',
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/approve`
    )
    const response = await axios.post(
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/approve`,
      {},
      {
        headers: {
          'Private-Token': gitlabToken,
        },
      }
    )
    return response.data
  } catch (error: any) {
    console.error(
      `Error fetching merge request: ${error.response.status} - ${error.response.statusText}`
    )
    console.error(error.response.data)
    throw error
  }
}

export const mergedMergeRequestById = async (mergeRequestId: string) => {
  try {
    console.log(
      '`${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/merge`',
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/merge`
    )
    const response = await axios.put(
      `${gitlabApiUrl}/projects/${encodeURIComponent(projectId)}/merge_requests/${mergeRequestId}/merge`,
      {},
      {
        headers: {
          'Private-Token': gitlabToken,
        },
      }
    )
    return response.data
  } catch (error: any) {
    console.error(
      `Error fetching merge request: ${error.response.status} - ${error.response.statusText}`
    )
    // console.error(error.response.data)
    throw error
  }
}
