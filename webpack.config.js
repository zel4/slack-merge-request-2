const path = require('path');
const nodeExternals = require('webpack-node-externals');
const { CleanWebpackPlugin }  = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin');


const {
    NODE_ENV = 'production',
} = process.env;

module.exports = {
    mode: NODE_ENV,
    entry: {
        "bundle": path.resolve(__dirname, 'src/index.ts')
    },
    target: 'node',
    externals: [nodeExternals()],
    optimization: {
        minimizer: [ new TerserPlugin({ extractComments: false})]
    },
    plugins: [
        new CleanWebpackPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    output: {
        // chunkFilename: '[name].js',
        // filename: '[name].js',
        filename: 'index.js',
        path: path.resolve(__dirname, 'bundle'),
    },
    node: {
        __dirname: false,
        __filename: false,
    },
    // devtool: 'source-map',
};
